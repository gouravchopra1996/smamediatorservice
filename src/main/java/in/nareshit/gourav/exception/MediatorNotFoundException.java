package in.nareshit.gourav.exception;

public class MediatorNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MediatorNotFoundException() {
		super();
	}

	public MediatorNotFoundException(String message) {
		super(message);
	}

}
