package in.nareshit.gourav.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.nareshit.gourav.entity.Mediator;
import in.nareshit.gourav.exception.MediatorNotFoundException;
import in.nareshit.gourav.service.IMediatorService;

@RestController
@RequestMapping("/mediator")
public class MediatorRestController {
	@Autowired
	private IMediatorService service;

	// 1. save mediator
	@PostMapping("/create")
	public ResponseEntity<String> createMediator(
			@RequestBody Mediator mediator) {
		ResponseEntity<String> response = null;
		Long id = service.saveMediator(mediator);
		response = ResponseEntity.ok("Mediator with '" + id + "' Created!");
		return response;
	}

	// 2. fetch all customers
	@GetMapping("/all")
	public ResponseEntity<List<Mediator>> getAllMediator() {
		ResponseEntity<List<Mediator>> response = null;
		List<Mediator> list = service.getAllMediator();
		response = ResponseEntity.ok(list);
		return response;
	}
	
	//3. fetch one customer by email
		@GetMapping("/find/{mail}")
		public ResponseEntity<Mediator> getOneMediatorByEmail(
				@PathVariable String mail
				) 
		{
			ResponseEntity<Mediator> response = null;
			try {
				Mediator med = service.getOneMediatorByEmail(mail);
				response = new ResponseEntity<Mediator>(med,HttpStatus.OK);
			} catch (MediatorNotFoundException cnfe) {
				cnfe.printStackTrace();
				throw cnfe;
			}
			return response;
		}
}
