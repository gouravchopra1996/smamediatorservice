package in.nareshit.gourav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmaMediatorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmaMediatorServiceApplication.class, args);
	}

}
