package in.nareshit.gourav.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "mediator_tab")
public class Mediator {
	@Id
	@Column(name="med_id_col")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="med_name_col")
	private String name;
	
	@Column(name="med_email_col")
	private String email;
	
	@Column(name="med_regNum_col")
	private String registerNumber;
	
	@Column(name="med_medCode_col")
	private String mediatorCode;
	
	@Column(name="med_addr_col")
	private String adress;
	
	@Column(name="med_serCode_col")
	private String serviceMode;
	
	@Column(name="med_status_col")
	private String status;
}
