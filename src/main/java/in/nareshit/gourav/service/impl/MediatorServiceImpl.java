package in.nareshit.gourav.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nareshit.gourav.entity.Mediator;
import in.nareshit.gourav.exception.MediatorNotFoundException;
import in.nareshit.gourav.repo.MediatorRepository;
import in.nareshit.gourav.service.IMediatorService;

@Service
public class MediatorServiceImpl implements IMediatorService {
	@Autowired
	private MediatorRepository repo;

	@Override
	public Long saveMediator(Mediator med) {
		return repo.save(med).getId();
	}

	@Override
	public Mediator getOneMediator(Long id) {
		Optional<Mediator> med = repo.findById(id);
		return validateInput(med, id.toString());
	}

	@Override
	public Mediator getOneMediatorByEmail(String email) {
		Optional<Mediator> med=repo.findByEmail(email);
		return validateInput(med, email);
	}

	@Override
	public List<Mediator> getAllMediator() {
		return repo.findAll();
	}

	private Mediator validateInput(Optional<Mediator> med, String input) {
		return med.orElseThrow(
				()-> new MediatorNotFoundException(
						"Customer '" + input + "' not found")
				);
	}
}
