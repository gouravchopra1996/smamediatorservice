package in.nareshit.gourav.service;

import java.util.List;

import in.nareshit.gourav.entity.Mediator;

public interface IMediatorService {
	Long saveMediator(Mediator med);
	Mediator getOneMediator(Long id);
	Mediator getOneMediatorByEmail(String email);
	List<Mediator> getAllMediator();
}
