package in.nareshit.gourav.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.gourav.entity.Mediator;

public interface MediatorRepository extends JpaRepository<Mediator, Long> {
	Optional<Mediator> findByEmail(String email);
}
